package com.jkb.enjoyor.sinaautomediaplay.application;

import android.app.Application;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

/**
 * Created by YuanYuan on 2016/9/7.
 */
public class MyApplication extends Application {
    public static MyApplication instance;
    public JCVideoPlayerStandard VideoPlaying;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
